#include "Vector.h"
#include <iostream>
using namespace std;

Vector::Vector(int n)
{
	if (n < 2)
	{
		n = 2;
	}
	_size = 0;
	_capacity = n;
	_resizeFactor = n;
	this->_elements = new int[this->_resizeFactor];
}

Vector::Vector(const Vector& other) : _elements(NULL)
{
	_size = other.size();
	_capacity = other.capacity();
	_resizeFactor = other.resizeFactor();
	this->_elements = new int[other.capacity()];
	for (int i = 0; i < _size; i++)
	{
		_elements[i] = other._elements[i];
	}
}

Vector :: ~Vector()
{
	if (this->_elements)
	{
		delete[] this->_elements;
	}

	this->_elements = NULL;
	this->_size = 0;
	this->_capacity = 0;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	return this->_size == 0;
}

void Vector::assign(const int val)
{
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}

void Vector::push_back(const int& val)
{
	if (this->_size == this->_capacity)// if the array is full
	{
		this->reserve(this->_resizeFactor + this->_capacity);
	}

	this->_elements[this->_size] = val;
	this->_size++;
}

int Vector::pop_back()
{
	if (_size > 0)
	{
		this->_size--;
		return _elements[_size];
	}
	else
	{
		cout << "error: pop from empty vector" << endl;
		return -9999;
	}
}

void Vector::reserve(const int n)
{
	if (n > this->_capacity)// if not enough capacity in the vector
	{
		int* temp = this->_elements;
		int cp = this->_capacity + this->_resizeFactor;
		while (n > cp)
		{
			cp += _resizeFactor;
		}
		this->_elements = new int[cp];
		this->_capacity = cp;
		for (int i = 0; i < this->_size; i++)
		{
			this->_elements[i] = temp[i];
		}
		delete[] temp;
	}
}

void Vector::resize(const int n)
{
	this->reserve(n);
	_size = n;
}

void Vector::resize(const int n, const int& val)
{
	int i;
	int start = _size;
	this->resize(n);
	for (i = start; i < _size; ++i)
	{
		this->_elements[i] = val;
	}
}

Vector& Vector :: operator=(const Vector& other)
{
	if (this != &other)
	{
		delete[] _elements;
	}
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;
	this->_elements = new int[this->_capacity];
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = other._elements[i];
	}
	return *this;
}

int& Vector :: operator[](int n) const
{
	if (n >= this->_size || n < 0)
	{
		cout << "An error occurred - index out of range" << endl;
		n = 0;
	}
	return this->_elements[n];
}
